from config import config, getLogger, loadYAML
import glob
import yaml
from jinja2 import Environment, FileSystemLoader
import os

moduleLogger = getLogger(__name__)

Env = Environment(loader=FileSystemLoader(config.template.dir))

def getTemplate(hostname, params):
	for key in params.keys():
		if os.path.isfile(config.template.dir + '/' + key + '.yml'):
			return key + '.yml'
	return 'default.yml'

def generateConfig(accessIP, params):
	hostnumber = str(int(accessIP.split('.')[3][-2:]) - 50).zfill(2)
	hostname = config.hostname.prefix + hostnumber
	moduleLogger.debug('Generating config for %s'%hostname)
	template = getTemplate(hostname, params)
	return Env.get_template(template).render(hostname = hostname + config.hostname.domain,
											 hostname_short = hostname,
											 ip_addr = accessIP )
