FROM jfloff/alpine-python:3.4-onbuild

RUN apk update && apk add openssh
COPY . /app

WORKDIR /app

CMD gunicorn -b 0.0.0.0:3113 zhinu:app
