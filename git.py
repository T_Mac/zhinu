import subprocess
import os
from config import config, getLogger
moduleLogger = getLogger(__name__)

install_path = os.path.dirname(os.path.realpath(__file__))
config_path = install_path + '/config'

def pullConfig():
	moduleLogger.info('Pulling config from %s'%config.git.repo)

	env = {'GIT_SSH_COMMAND' :'ssh -i %s/%s'%(install_path, config.git.ident)}
	if not os.path.isdir(config_path):
		subprocess.Popen(['/usr/bin/git', 'clone', config.git.repo, config_path], env=env)
	else:
		subprocess.Popen(['/usr/bin/git', 'pull'], cwd = config_path, env = env)
