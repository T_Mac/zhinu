from config import config, getLogger
import falcon
from generate import generateConfig
from git import pullConfig
moduleLogger = getLogger(__name__)

class ConfigLoader:
	def on_get(self, req, resp):
		"""Creates colud configs"""
		resp.status = falcon.HTTP_200

		moduleLogger.info('Serving config to %s'%(req.access_route[0]))

		resp.body = generateConfig(req.access_route[0], req.params)

class IgnitionLoader:
	def on_get(self, req, resp):
		"""Creates colud configs"""
		resp.status = falcon.HTTP_200

		moduleLogger.info('Serving ignition to %s'%(req.access_route[0]))
		with open('templates/ignition.json') as f:
			resp.body = f.read()

class ConfigChanged:
	def on_post(self, req, resp):
		"""Recieves post when config repo has changed, triggering a pull"""
		resp.status = falcon.HTTP_200
		resp.body = 'success'
		moduleLogger.debug('Update webhook triggered')
		pullConfig()

pullConfig()
app = falcon.API()

config_loader = ConfigLoader()
ignition_loader = IgnitionLoader()
config_changed = ConfigChanged()

app.add_route('/config', config_loader)
app.add_route('/ignition', ignition_loader)
app.add_route('/update', config_changed)
